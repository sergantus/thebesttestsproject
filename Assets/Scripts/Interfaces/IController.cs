﻿using UnityEngine.PlayerLoop;

namespace Interfaces
{
    interface IController
    {
        void InitController(params object[] initParams);
    }
}