﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace MyUtils
{
    public enum AudioType
    {
        SFX,
        MUS
    }
    public class Audio : MonoBehaviour
    {
        private static readonly List<string> SOUND_NAMES = new List<string>()
        {
            "Button",
            "Button2",
            "Catch",
            "Lose",
            "LostLife",
            "Mode",
            "female0",
            "female1",
            "female2",
            "WashHands"
        };

        private float MUSIC_VOLUME = 1f;
        private float SFX_VOLUME = 1f;

        private AudioClip _gameMusic;
        private AudioSource _sourceSfx;
        private AudioSource _sourceMusic;

        private List<AudioClip> _clips = new List<AudioClip>();
        
        private Dictionary<string, AudioSource> _audioSources = new Dictionary<string, AudioSource>();

        public void InitializeAudioClips(GameObject container)
        {
            for (int i = 0; i < SOUND_NAMES.Count; i++)
            {
                SOUND_NAMES[i] = Path.GetFileNameWithoutExtension(SOUND_NAMES[i]);
                AudioClip clip = Resources.Load<AudioClip>("Sounds/" + SOUND_NAMES[i]);
                _clips.Add(clip);
                AudioSource source = container.AddComponent<AudioSource>();
                source.clip = clip;

                _audioSources.Add(SOUND_NAMES[i], source);
            }
        }

        public void OnePlaySource(string name, float volume)
        {
            AudioSource source = _audioSources[name];
            source.Stop();
            source.loop = false;
            source.volume = volume;
            source.Play();
        }
        
        public void LoopPlaySource(string name, float volume)
        {
            AudioSource source = _audioSources[name];
            source.Stop();
            source.loop = true;
            source.volume = volume;
            source.Play();
        }
        
        public void StopSource(string name)
        {
            AudioSource source = _audioSources[name];
            source.Stop();
        }
        
        public void StopAllSource()
        {
            foreach(KeyValuePair<string, AudioSource> entry in _audioSources)
            {
                entry.Value.Stop();
            }
        }

        public AudioSource AddAudioSource(GameObject target, AudioType type, string name)
        {
            AudioSource source = target.AddComponent<AudioSource>();
            source.clip = GetAudioClip(name);
            
            if (type == AudioType.MUS)
            {
                source.loop = true;
                source.volume = MUSIC_VOLUME;
                return source;
            }
            
            source.loop = false;
            source.volume = SFX_VOLUME;
            return source;
        }

        public AudioClip GetAudioClip(string name)
        {
            return _clips.FirstOrDefault(c => c.name == name);
        }
    }
}
