﻿using System;
using System.Linq;

namespace MyUtils
{
    public static class StringUtils
    {
        private static readonly string[] TRUE_VALUES = new[] {"1", "true", "yes", "y", "on", "enabled"};
        private static readonly Random RANDOM = new Random();
        
        const string CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        
        public static string Truncate(this string value, int maxLength, string replace = null)
        {
            if (value.Length <= maxLength) {
                return value;
            }
            if (string.IsNullOrEmpty(replace)) {
                return value.Substring(0, maxLength);
            }
            return value.Substring(0, maxLength - replace.Length) + replace;
        }

        public static bool ParseBoolean(string str)
        {
            return Array.Find(TRUE_VALUES, s => s == str.ToLower()) != null;
        }

        public static string Capitalize(string word)
        {
            return word.Substring(0, 1).ToUpper() + word.Substring(1).ToLower();
        }
        
        public static string RandomString(int length)
        {
            return new string(Enumerable.Repeat(CHARS, length)
                                        .Select(s => s[RANDOM.Next(s.Length)])
                                        .ToArray());
        }
    }
}