﻿using System.Linq;
using UnityEngine;

namespace MyUtils
{
    public class FullAreaResizerComponent : MonoBehaviour
    {
        private Vector2 _resolution;

        private void Start()
        {
            SaveResolution();
            ResizeToScreenHeight();
        }

        private void Update()
        {
#if !UNITY_EDITOR
                return;
#endif
            CheckResolutionChanged();
        }

        private void CheckResolutionChanged()
        {
            int screenWidth = UnityEngine.Screen.width;
            int screenHeight = UnityEngine.Screen.height;

            if (_resolution.x != screenWidth || _resolution.y != screenHeight) {
                ResizeToScreenHeight();
                SaveResolution();
            }
        }

        private void SaveResolution()
        {
            _resolution = new Vector2(UnityEngine.Screen.width, UnityEngine.Screen.height);
        }

        private void ResizeToScreenHeight()
        {
            RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
            if (rectTransform == null) {
                return;
            }

            float screenWidth = Screen.width;
            float screenHeight = Screen.height;

            float localWidth = rectTransform.rect.width;
            float localHeight = rectTransform.rect.height;
            
            float screenAspect = screenWidth / screenHeight;
            float localAspect = localWidth / localHeight;

            float uiHeightScale = GetUIHeightScale();

            float heightResizeCoefficient = screenHeight / localHeight;
            float widthResizeCoefficient = screenWidth / localWidth;

            float resizeCoefficient = heightResizeCoefficient > widthResizeCoefficient ? heightResizeCoefficient : widthResizeCoefficient;

            resizeCoefficient /= uiHeightScale;
            
            /*if (screenWidth < localWidth) {
                resizeCoefficient += localAspect - screenAspect;
            }*/
            
            rectTransform.localScale = new Vector3(resizeCoefficient, resizeCoefficient, 1F);
        }

        private float GetUIHeightScale()
        {
            RectTransform uiRectTransform = GetUIRectTransform();
            if (uiRectTransform == null) {
                return 1F;
            }

            return uiRectTransform.localScale.y;
        }

        private RectTransform GetUIRectTransform()
        {
            RectTransform[] parentRectTransforms = GetComponentsInParent<RectTransform>();
            return parentRectTransforms.First(rt => rt.gameObject.name == "UI");
        }
    }
}