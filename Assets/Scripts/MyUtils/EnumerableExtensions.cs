﻿using System;
using System.Collections.Generic;

namespace MyUtils
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> self, Action<T> action)
        {
            foreach (T item in self) {
                action(item);
            }
        }
        
        public static T RandomEnumValue<T> ()
        {
            var v = Enum.GetValues (typeof (T));
            return (T) v.GetValue (new Random ().Next(v.Length));
        }
    }
}