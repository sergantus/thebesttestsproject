﻿using Interfaces;
using UnityEngine;

namespace MyUtils
{
    public class PrefabBuilder : MonoBehaviour
    {
        public static T Create<T>(string path, Transform container, params object[] initParams) where T : Component
        {
            GameObject prefab = Instantiate(Resources.Load(path), container) as GameObject;
            Component controller = prefab.AddComponent(typeof(T));
            IController init = (IController) controller;
            init.InitController(initParams);
            
            return (T) controller;
        }
        
        

        
    }
}
