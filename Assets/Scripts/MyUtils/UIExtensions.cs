﻿using UnityEngine;
using UnityEngine.UI;

namespace MyUtils
{
    public static class UIExtensions
    {
        public static void SetAlpha(this Graphic g, int alpha)
        {
            Color color = g.color;
            color.a = alpha;
            g.color = color;
        }
    }
}