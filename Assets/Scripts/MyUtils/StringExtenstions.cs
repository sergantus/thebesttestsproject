﻿using System.Collections.Generic;
using System.Text;

namespace MyUtils
{
    public static class StringExtenstions
    {
        private static readonly Dictionary<char, string> TRANSLIT = new Dictionary<char, string> {
                {'а', "a"},
                {'б', "b"},
                {'в', "v"},
                {'г', "g"},
                {'д', "d"},
                {'е', "e"},
                {'ё', "e"},
                {'ж', "zh"},
                {'з', "z"},
                {'и', "i"},
                {'й', "y"},
                {'к', "k"},
                {'л', "l"},
                {'м', "m"},
                {'н', "n"},
                {'о', "o"},
                {'п', "p"},
                {'р', "r"},
                {'с', "s"},
                {'т', "t"},
                {'у', "u"},
                {'ф', "f"},
                {'х', "kh"},
                {'ц', "ts"},
                {'ч', "ch"},
                {'ш', "sh"},
                {'щ', "shch"},
                {'ъ', ""},
                {'ы', "y"},
                {'ь', ""},
                {'э', "e"},
                {'ю', "yu"},
                {'я', "ya"}
        };

        public static string Transliterate(this string input)
        {
            string lower = input.ToLower();
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < lower.Length; i++) {
                char letter = lower[i];
                result.Append(TRANSLIT.ContainsKey(letter) ? TRANSLIT[letter] : letter.ToString());
            }
            return result.ToString();
        }
    }
}