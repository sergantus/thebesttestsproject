﻿using System.Collections.Generic;
using UnityEngine;

namespace MyUtils
{
    public static class SpriteUtil
    {
        private static readonly Dictionary<string, Sprite[]> _atlases = new Dictionary<string, Sprite[]>();
        private static readonly Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();

        public static Sprite GetSprite(string name)
        {
            Sprite item;

            if (_sprites.ContainsKey(name)) {
                item = _sprites[name];
            } else {
                item = Resources.Load<Sprite>("Sprites/" + name);
                _sprites.Add(name, item);
            }

            if (item == null) {
                return null;
            }
            
            if (item.name.Equals(name)) {
                return item;
            }

            return null;
        }
    
        public static Sprite GetSpriteFromAtlas(string atlas, string spriteName)
        {
            Sprite[] items;

            if (_atlases.ContainsKey(atlas)) {
                items = _atlases[atlas];
            } else {
                items = Resources.LoadAll<Sprite>("Sprites/" + atlas);
                _atlases.Add(atlas, items);
            }

            if (items == null) {
                return null;
            }

            foreach (Sprite item in items) {
                if (item.name.Equals(spriteName)) {
                    return item;
                }
            }

            return null;
        }
    }
}
